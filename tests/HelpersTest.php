<?php


use App\Helpers\ConfigHelper;

class HelperTest extends TestCase
{
    public function testCoordinates()
    {
        $goodCoords = [
            '12.345678 12.345678' => [12.345678, 12.345678],
//            '12 34 56 78 12 34 56 78' => [12.58243889, 12.582438898],
            '12°34\'56.78"N 12°34\'56.78"E' => [12.58243889, 12.58243889]
        ];
        foreach ($goodCoords as $text => $actual){
            $data = getCoordinates($text);
            $this->assertInternalType('array',$data);
            $this->assertEquals(count($data), 2);
            $this->assertTrue(is_float($data[0]));
            $this->assertTrue(is_float($data[1]));
            foreach ($data as $i => $check){
                $this->assertEquals(round($data[$i], 4), round($actual[$i], 4));
            }
        }
        $data = getCoordinates('3 4');
    }

    /**
     * @dataProvider urlValidDataSet
     *
     * @param $url
     * @param $expected
     *
     * @throws \App\Exceptions\UrlParseException
     */
    public function testValidParser($url, $expected) {
        $result = ConfigHelper::parseUrl($url);
        foreach ($expected as $key => $value) {
            $this->assertEquals($value, array_get($result, $key));
        }

    }

    /**
     * @dataProvider urlInValidDataSet
     *
     * @param $url
     * @param $expected
     */
    public function testInValidParser($url, $expected) {
        $this->assertEquals($url, $expected);
    }

    public function urlValidDataSet() {
        return [
            'EnWithOutGameId' => [ 'http://msk.en.cx' , ['project' => 'Encounter', 'url' => 'http://msk.en.cx/', 'gameId' => null] ],
            'EnWithOutGameId' => [ 'sibkrsk.en.cx/GameDetails.aspx?gid=60364' , ['project' => 'Encounter', 'url' => 'http://sibkrsk.en.cx/', 'gameId' => 60364] ],
            'enWithGameId'    => [ 'http://demo.en.cx/GameDetails.aspx?gid=26661' , ['project' => 'Encounter', 'url' => 'http://demo.en.cx/', 'gameId' => 26661] ],
            'enWithOutHttp'    => [ 'demo.en.cx/GameDetails.aspx?gid=26661' , ['project' => 'Encounter', 'url' => 'http://demo.en.cx/', 'gameId' => 26661] ],
            'enWithOutHttp1'    => [ 'demo.en.cx/' , ['project' => 'Encounter', 'url' => 'http://demo.en.cx/', 'gameId' => null] ],
            'enWithOutHttp2'    => [ 'demo.en.cx' , ['project' => 'Encounter', 'url' => 'http://demo.en.cx/', 'gameId' => null] ],
            'enWithOutHttp3'    => [ 'demo.en.cx/calendar.aspx' , ['project' => 'Encounter', 'url' => 'http://demo.en.cx/', 'gameId' => null] ],
            'enWithOutHttp4'    => [ 'DeMo.en.cx/cAlendar.aspx' , ['project' => 'Encounter', 'url' => 'http://demo.en.cx/', 'gameId' => null] ],

            'Dozor' => [ 'http://lite.dzzzr.ru/msk' , ['project' => 'DozorLite', 'url' => 'http://lite.dzzzr.ru/'] ],
            'DozorWithPin'    => [ 'http://demo.en.cx/GameDetails.aspx?gid=26661' , ['project' => 'Encounter', 'url' => 'http://demo.en.cx/', 'gameId' => 26661] ],

            'Redfox' => [ 'www.redfoxkrsk.ru/login' , ['project' => 'RedfoxAvangard', 'url' => 'http://www.redfoxkrsk.ru/'] ],
            'RedfoxCustom' => [ 'http://tomsk.redfoxkrsk.ru/login' , ['project' => 'RedfoxAvangard', 'url' => 'http://tomsk.redfoxkrsk.ru/'] ],
            'RedfoxOneMore' => [ 'http://nsk.redfoxkrsk.ru/login' , ['project' => 'RedfoxAvangard', 'url' => 'http://nsk.redfoxkrsk.ru/', 'gameId' => null] ],
        ];
    }

    public function urlInValidDataSet() {
        return [
//            [3333, 21]
        ];
    }

}