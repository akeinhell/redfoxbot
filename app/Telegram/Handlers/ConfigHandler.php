<?php


namespace App\Telegram\Handlers;

use App\Exceptions\UrlParseException;
use App\Helpers\ConfigHelper;
use App\Telegram\Bot;
use App\Telegram\Commands\ConfigCommand;
use App\Telegram\Config;
use TelegramBot\Api\Types\Update;

class ConfigHandler extends BaseHandler
{
    /**
     * @param Update $update
     *
     * @return bool
     * @throws \TelegramBot\Api\Exception
     * @throws \TelegramBot\Api\InvalidArgumentException
     */
    public function run(Update $update)
    {
        $chatId = $update->getMessage()->getChat()->getId();
        $state  = Config::getState($chatId) ?: '';
        list(, $type) = explode(':', $state);
        $text = $update->getMessage()->getText();

        $message = '';

        switch ($type) {
            case 'url':
                try {
                    $result = ConfigHelper::parseUrl($text);
                    $message = array_pull($result, 'message');

                    foreach ($result as $key => $value) {
                        Config::setValue($chatId, $key, $value);
                    }
                } catch (UrlParseException $e) {
                    \Log::error('Failed parse url ', [
                        'text'=>$text,
                        'message' => $e->getMessage()
                    ]);
                    $message = $e->getMessage();
                }
                break;
            // todo may be easy way?!
            case 'pin':
            case 'teamPassword':
            case 'teamLogin':
            case 'login':
            case 'password':
            case 'gameId':
                Config::setValue($chatId, $type, $update->getMessage()->getText());
                $message = 'Настройки обновлены';
                break;
            default:
                Bot::sendMessage($chatId, 'unknown state ' . $state);
        }

        if ($update->getMessage()->getChat()->getType() !== 'private') {
            try {
                Bot::action()->deleteMessage($chatId, $update->getMessage()->getMessageId());
            } catch (\Exception $e) {
                \Log::error('Cannot delete msg:',  ['message' => $e->getMessage()]);
            }
        }
        if ($message) {
            Bot::sendMessage($chatId, $message, ConfigCommand::getConfigKeyboard($chatId));
        }
        Config::setState($chatId, '');

        return false;
    }
}
