<?php

namespace App\Jobs;

use App\Games\BaseEngine\AbstractGameEngine;
use App\Jobs\Job;
use App\Telegram\Bot;
use App\Telegram\Config;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TrackingJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $chatId;

    /**
     * Create a new job instance.
     *
     * @param int $chatId
     */
    public function __construct(int $chatId)
    {
        $this->chatId = $chatId;
    }

    /**
     * @param $chatId
     *
     * resolve engine
     * @return AbstractGameEngine
     * @throws \Exception
     */
    private function getEngine($chatId): AbstractGameEngine
    {
        $config = Config::get($chatId);
        if (!$config || !$config->project) {
            throw new \Exception('Setting is not available');
        }
        $projectClass = '\\App\\Games\\Engines\\' . $config->project . 'Engine';

        /* @var AbstractGameEngine $engine */
        return new $projectClass($chatId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $cacheKey = 'TRACK:' . $this->chatId;

            $engine          = $this->getEngine($this->chatId);
            $actualLevelList = $engine->getQuestList();

            $oldLevels = \Cache::get($cacheKey, []);

            $diffKeys = array_diff($actualLevelList, $oldLevels);
            if ($diffKeys) {
                $newSet = $oldLevels + $diffKeys;
                \Cache::put($cacheKey, $newSet, 60 * 24 * 7);
                Bot::action()->sendMessage($this->chatId, $this->formatMessage($diffKeys));
            }
        } catch (\Exception $e) {
            \Log::warning('remove chat: ' . $this->chatId . ': ' . $e->getMessage());
        }

        dispatch((new TrackingJob($this->chatId))->delay(60));
    }

    private function formatMessage(array $diffKeys)
    {
        return '#Вброс' . PHP_EOL . 'Новые задания: ' . PHP_EOL . implode(PHP_EOL, $diffKeys);
    }
}
