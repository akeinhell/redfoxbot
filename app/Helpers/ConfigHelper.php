<?php


namespace App\Helpers;

use App\Exceptions\UrlParseException;
use League\Uri\Components\Host;
use League\Uri\Components\Query;
use League\Uri\Parser;

class ConfigHelper
{
    /**
     * @param $host
     * @param $domain
     *
     * @return string
     * @throws UrlParseException
     */
    private static function resolveDozorProject(Host $host, $domain):string {
        switch (true) {
            case $host->getSubdomain() === 'classic':
//                        $project = 'DozorClassic';
                throw new UrlParseException('DozorClassic Пока не поддерживается');
            case $domain == 'dzzzr.ru':
                return 'DozorLite';
            default:
                return 'Ekipazh';
        }
    }
    /**
     * @param string $text
     *
     * @return array
     * @throws UrlParseException
     */
    public static function parseUrl(string $text): array
    {
        \Log::debug('parseUrl: ', compact('text'));

        if (strpos($text, 'http') === false) {
            $text = 'http://' . $text;
            \Log::debug('append http scheme : ',  compact('text'));
        };
        $uriParser = new Parser();
        try {
            $uri          = $uriParser($text);
            $query        = new Query(array_get($uri, 'query', ''));
            $parsedDomain = array_get($uri, 'host', '');

            $host = new Host($parsedDomain);
        } catch (\Exception $e) {
            \Log::error('invalidUrl: ', compact('text'));
            throw new UrlParseException('Прислан не валидный url: ' . $text);
        }
        $domain = $host->getRegisterableDomain();
        $city   = current(array_pad(explode('/', trim($uri['path'], '/')), 1, ''));

        if (!$domain) {
            \Log::error('invalidDomain: ', compact('text'));
            throw new UrlParseException('Ошибка парсинга URL (не правильный домен)');
        }

        $result = [
            'url' => sprintf('http://%s/', $host->__toString())
        ];

        switch ($domain) {
            case 'dzzzr.ru':
            case 'ekipazh.org':
                if (!$city) {
                    \Log::error('invalidCity: ' , compact('text'));
                    throw new UrlParseException('Ошибка парсинга URL (не указан город)');
                }

                $result['project'] = static::resolveDozorProject($host, $domain);
                $result['domain'] = $city;
                $result['pin'] = $query->getParam('pin');
                $result['message'] = 'настройки установлены. Не забудьте указать пин-код';
                break;
            case 'redfoxkrsk.ru':
                $result['project'] = 'RedfoxAvangard';
                $result['message'] = 'настройки установлены. Не забудьте указать логин/пароль';
                break;
            case 'en.cx':
            case 'quest.ua':
                $result['project'] = 'Encounter';
                $result['gameId'] = $query->getParam('gid');
                $result['message'] = 'настройки установлены. Не забудьте указать логин/пароль';
                break;
            case 'probeg.net.ua':
                if (!$city) {
                    \Log::error('invalidCity: ', compact('text'));
                    throw new UrlParseException('не удалось распознать ID игры');
                }
                $result['url'] = $result['url'] . $city . '/';
                $result['project'] = 'Probeg';
                $result['pin'] = $query->getParam('p');
                $result['message'] = 'настройки установлены. Не забудьте указать пин-код';
                break;
            default:
                \Log::error('invalidURL: ', compact('text'));
                throw new UrlParseException('Не удалось распознать присланный адрес. ' . PHP_EOL . 'выберите пункт url заново');
        }

        return $result;
    }
}