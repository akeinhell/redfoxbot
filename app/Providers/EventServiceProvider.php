<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [];

    /**
     * Register any other events for your application.
     *
     * @param \Illuminate\Contracts\Events\Dispatcher $events
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
        \Log::listen(function ($level, $message, $context) {
            if ($level == 'debug') {
                $level = 'info';
            }
            if (!is_array($context)) {
                $context = [$context];
            }
            $context['level'] = $level;
            if ($message instanceof \Exception) {
                app('sentry')->captureException($message, [], $context);
            } else {
                app('sentry')->captureMessage($message, [], $context);
            }
        });
    }
}
