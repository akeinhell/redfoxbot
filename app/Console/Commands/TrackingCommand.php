<?php

namespace App\Console\Commands;

use App\Games\BaseEngine\AbstractGameEngine;
use App\Telegram\Bot;
use App\Telegram\Config;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Console\Command;

/**
 * @deprecated use Tracking Job
 * Class TrackingCommand
 * @package App\Console\Commands
 */
class TrackingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tracking:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'tracking:run';

    private function getEngine($chatId): AbstractGameEngine
    {
        $config = Config::get($chatId);
        if (!$config || !$config->project) {
            throw new \Exception('Setting is not available');
        }
        $projectClass = '\\App\\Games\\Engines\\' . $config->project . 'Engine';

        /* @var AbstractGameEngine $engine */
        return new $projectClass($chatId);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chats = \Track::getChatList();

        $isNotified = false;
        $client = new Client([
            'base_uri' => 'https://api.voximplant.com/',
        ]);
        $phones = [
            '89095381635',
            '89138282454',
            '89609764848',
            '89138238393',

            '+79994419371'
        ];

        foreach ($chats as $chatId) {
            try {
                $cacheKey = 'TRACK:' . $chatId;
                $this->info('scan ' . $cacheKey);
                $engine          = $this->getEngine($chatId);
                $actualLevelList = $engine->getQuestList();
                if (!$actualLevelList) {
                    throw new \Exception('Не найдено списка заданий');
                }

                $oldLevels = \Cache::get($cacheKey, []);

                $diffKeys = array_diff($actualLevelList, $oldLevels);
                if ($diffKeys) {
                    if (!$isNotified && $chatId == -1001298831045) {
                        $requests = array_map(function ($phone) use ($client) {
                            return $client->requestAsync('GET', 'platform_api/StartScenarios/', [
                                'query' => [
                                    'account_id' => '595863',
                                    'rule_id' => '1371832',
                                    'api_key' =>  'd618d2f3-a368-47b4-80ce-1f5f09a40a8c',
                                    'script_custom_data' => $phone
                                ]
                            ]);
                        }, $phones);

                        try {
                            $res = Promise\settle($requests)->wait();
                        } catch (\Exception $e) {
                            $this->error($e->getMessage());
                        }

                        $isNotified = true;
                    }
                    $newSet = $oldLevels + $diffKeys;
                    \Cache::put($cacheKey, $newSet, 60 * 24 * 7);
                    Bot::action()->sendMessage($chatId, $this->formatMessage($diffKeys));
                }
            } catch (\Exception $e) {
                $this->warn('remove chat: ' . $chatId . ': ' . $e->getMessage());
                try {
                    \Track::removeChat($chatId, $e->getMessage());
                } catch (\Exception $e) {
                    $this->error($e->getMessage());
                }
            }
        }
    }

    private function formatMessage(array $diffKeys)
    {
        return '#Внимание' . PHP_EOL . 'Новые задания: ' . PHP_EOL . implode(PHP_EOL, $diffKeys);
    }
}
